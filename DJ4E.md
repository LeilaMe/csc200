# DJ4E Study Guide One
## Dynamic Web Content
### Leila Meng 
---

*Question One*
1. What is a protocol?
- A protocol is a set of rules that control the exchange and transmission of data between devices. 
2. Why does Dr. Chuck say it is important to understand web protocols before learning to be a web developer?
- To become a really good one, also for debugging. 
3. What is **HTTP**?
- It stands for Hyper Text Transfer Protocol and is the communications protocol for connecting to web servers and sending HTML pages back to the browser.

*Question Two*
1. Describe the three parts...
- on the left is a web page which part of a website. It has an address.
- In the middle is a browser application, it is on the local computer. Along with webpages, they define the look and feel. 
- Internet
- Remote webserver. Backend technology. Django. makes requests and gets responses from server. 

*Question Three*
1. Describe what happens when you click on a link?
- **basic request/response cycle** asks for a document, retrieves the document, shows the document
- First you click, the browser application responds to events like mouse clicks,

 it opens a network socket to the webserver and requests the server to go here with git commmand and url, 

 the server does webserver stuff and sends a reponse back through the socket, returns a HTML document,

 the browser formats and displays the document to the user. 

*Question Four*
1. What is a **URL**?
- Uniform Resource Locator

**Three Great Parts**
1. allowed for multiple protocols
2. a host, domain name, server address, ip address
3. a document in the server
**it is the greatest thing ever because many engineers could use it because it was simple but they could change it to use it however they needed.**

*Question Five* 
1. What is an RFC?
- Request for comments. There is always room for improvement. no perfect design
2. What does Dr. Chuck say is important about the culture surrounding RFCs that helps make the internet what it is?
- everyone has an equal chance, it is free liscence free royalty free, no one owns the internet

*Question Six*
1. HTTP response 200
- a status line you have a document OK
2. HTTP response 404
- webpage not found doesnt exist

*Question Seven*
1. Dr. Chuck says he is a big fan of using the terminal ;-; why does he say this?
- more acessible and awesome, GUIs distract from simplicity of computers, 
2. What benefits does he say it provides?
- knowing how to start the server, debug applications, find log files in the command line is important in real life

*Question Eight*
1. Dr. Chuck tells us that \_\_\_\_\_\_\_\_\_\_ was the conceptual model used as the concept of computer networked communications was developed in the 1960s.
Network socket phone calls

*Question Nine*
1. What is a socket?
It is a temporary connection between computers and data. Makes a phone call, transfers data, disconnects. The softwares are communicating, sometimes the software denies you access. The internet lets the softwares talk. 

*Question Ten*
1. What is a **TCP port number**?
helps you identify. Part of an ip address. An extension, there are expected protocols
\n done \n a
