#!/usr/bin/env python3
import os
import sys

#from base64 import Base64Converter

class Base64Converter:
    def __init__(self):
        """
        Create a string containing the Base64 digits for encoding and a
        dictionary containing the numerical value of each digit charcter
        for decoding.
        """
        self.digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.digits += 'abcdefghijklmnopqrstuvwxyz0123456789+/'
        self.digpos = {}
        for pos, dig in enumerate(self.digits):
            self.digpos[dig] = pos
    def main():
        try:
            infile = open(sys.argv[1], 'r')
        except IndexError:
            print('Requires file to decode as argument.')
            quit()
        except FileNotFoundError:
            print(f'{sys.argv[1]} file not found.')
            quit()

        outfile = open(sys.argv[1][:-7].replace('__', '.'), 'wb')
        b64 = Base64Converter()
        outfile.write(b64.decode(infile))
    
        infile.close()
        outfile.close()

    def encode3bytes(self, bytes3):
        """
        Convert 3 bytes into 4 base64 encoded characters. Pads with = or == when
        it recieves only 1 or 2 bytes as an argument respectively.
        
        >>> encode3bytes(b'\\x5A\\x2B\\xE6')
        'Wivm'
        >>> encode3bytes(b'\\x49\\x33\\x8F')
        'STOP'
        >>> encode3bytes(b'\\xFF\\xFF\\xFF')
        '////'
        >>> encode3bytes(b'\\x00\\x00\\x00')
        'AAAA'
        >>> encode3bytes(b'T')
        'VA=='
        >>> encode3bytes(b'Te')
        'VGU='
        >>> encode3bytes(b'Tst')
        'VHN0'
        >>> encode3bytes(b'\\x00')
        'AA=='
        >>> encode3bytes(b'\\x00\\x00')
        'AAA='
        >>> encode3bytes(b'\\xFF')
        '/w=='
        >>> encode3bytes(b'\\xFF\\xFF')
        '//8='
        >>> encode3bytes('bTooManyBytes')
        Traceback (most recent call last):
            ...
        ValueError: Input should be 1 to 3 bytes
        >>> encode3bytes(['wrong', 'type', 42])
        Traceback (most recent call last):
            ...
        ValueError: Input should be 1 to 3 bytes
        """
        if not isinstance(bytes3, bytes) or len(bytes3) < 1 or len(bytes3) > 3:
            raise ValueError("Input should be 1 to 3 bytes")
        b1 = bytes3[0]
        index1 = b1 >> 2
        if len(bytes3) == 1:
            index2 = (b1 & 3) << 4
            return f'{self.digits[index1]}{self.digits[index2]}=='
        b2 = bytes3[1]
        index2 = (b1 & 3) << 4 | b2 >> 4
        if len(bytes3) == 2:
            index3 = (b2 & 15) << 2
            return f'{self.digits[index1]}{self.digits[index2]}{self.digits[index3]}='
        b3 = bytes3[2]
        index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
        index4 = b3 & 63
        return f'{self.digits[index1]}{self.digits[index2]}{self.digits[index3]}{self.digits[index4]}'
    
    def decode4chars(s, digits):
        """
        Return 4 base64 encoded characters to 3 bytes from which they orginated.
        Handle special endings == and = where only 1 or 2 bytes are returned
        repectively.

          >>> ds = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
          >>> ds += 'abcdefghijklmnopqrstuvwxyz0123456789+/'
          >>> digits = {}
          >>> for pos, digit in enumerate(ds):
          ...     digits[digit] = pos
          >>> decode4chars('STOP', digits)
          b'I3\\x8f'
          >>> decode4chars('Wivm', digits)
          b'Z+\xe6'
          >>> decode4chars('////', digits)
          b'\xff\xff\xff'
          >>> decode4chars('VA==', digits)
          b'T'
          >>> decode4chars('VGU=', digits)
          b'Te'
          >>> decode4chars('AA==', digits)
          b'\\x00'
          >>> decode4chars('AAA=', digits)
          b'\\x00\\x00'
          >>> decode4chars('/w==', digits)
          b'\\xff'
          >>> decode4chars('//8=', digits)
          b'\\xff\\xff'
          >>> decode4chars(42, digits)
      Traceback (most recent call last):
          ...
      ValueError: 42 is not a base64 encoded string
      >>> decode4chars('Not!', digits)
      Traceback (most recent call last):
          ...
      ValueError: Not! is not a base64 encoded string
    """
    if not isinstance(s, str) or len(s) != 4 or \
            not all([ch in self.digits for ch in s[:2]]) or \
            not all([ch in self.digits + '=' for ch in s[2:]]):
        raise ValueError(f'{s} is not a base64 encoded string')

    # Compute first byte
    int1 = self.digits[s[0]]
    int2 = self.digits[s[1]]
    b1 = (int1 << 2) | ((int2 & 48) >> 4)

    # Handle single byte return case
    if s[2:] == '==':
        return bytes([b1])

    int3 = self.digits[s[2]]
    b2 = (int2 & 15) << 4 | int3 >> 2

    # Handle 2 byte return case
    if s[3:] == '=':
        return bytes([b1, b2])

    int4 = digits[s[3]]
    b3 = (int3 & 3) << 6 | int4

    return bytes([b1, b2, b3])

if __name__ == '__main__':
    import doctest
    doctest.testmod()


