#the sum of a number to ten
def func(a):
    if a == 10:
        return 10
    if a < 10:
        return a + func(a+1)
    else:
        print("please a number less than 10")
print(func(-4))

#loops to add until it gets to two
def afunction(a):
    if a >= 2:
        if a == 2:
            return 0
        else:
            return a + afunction(a-1)
    else:
        print("please, put a number greater than or equal to two")
print(afunction(8))

#an indirect recursion
def haseven(x):
    if len(x) == 0:
        return True
    else:
        return hasodd(x[:-1])
def hasodd(x):
    return not haseven(x)
print(haseven("dogs and cats"))
print(hasodd("dogs and cats"))

#uses in to checks if it contains an item, like a list does
a = {1, 2, 3}
b = set(["dog", "cat", "zigzagoon"])
print(2 in a)
print("dog" not in b)

#testing pop
a_set = {1, 2, 3, 2, 5}
print(a_set)
a_set.add(4)
print(a_set)
a_set.pop()
print(a_set)

#set test again
def check(s):
    if "good" not in s:
        while len(s) > 0:
            s.pop()
        return s
    else:
        return "good, good, good"
print(check({1, 3, "food"}))
print(check({"good", 4, "100"}))

#set operations
food_available = {"banana", "apple", "chocolate", "peanut butter"}
dog = {"peanut butter", "dogfood", "water"}
def safefordog():
    return food_available & dog
def notsafe():
    return food_available - dog
print(safefordog())
print(notsafe())

#itertools functions test
from itertools import count, cycle, repeat
var = 0
for i in count(4):
    print(2*i)
    if i > 7:
        break
for e in cycle("dogs"):
    print(e)
    var += 1
    if var == 8:
        break
while True:
    repeat(print("ah"))
    if 1 == 1:
        break

#itertools function test on iterables
from itertools import accumulate, takewhile, chain
print(list(accumulate(range(4))))
print(list(takewhile(lambda x: x<3, range(5))))
print(list(chain(range(3), range(8, 17, 2))))

#testing combinatoric functions
from itertools import product, permutations
flavors = ("vanilla", "strawberry", "chocolate", "dirt")
toppings = ("frosting", "gummy bears", "potato chips")
print(list(product(flavors, range(3))))
print(list(permutations(toppings)))
print(list(product(flavors, toppings)))#a success!

# more pop tests
my_set = {1, 2, 3, 4, "pickles", 5}
print(my_set)
print(my_set.pop())
#always pops the first one
my_list = [1, 2, 3, 4, "cats", 100]
print(my_list)
print(my_list.pop())
#always pops the last one
myset = {"dog", "lizard", "arctic tern"}
print(myset)
myset = myset.pop()
print(myset)
# i have observed that pop will always take the first one but the first one can be random, it can be how the set was written or alphabetically or something else. 
