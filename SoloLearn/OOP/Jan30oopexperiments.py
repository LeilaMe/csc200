a = 2
b = a
c = a + b
print(c)
del a
print(c)
del b
print(c)
a = 3
print(c)

class AAaa:
    def __init__(self):
        self._cat = "moo"
frog = AAaa()
print(frog)
print(frog._cat)

class Woof:
    __ribbit = "eee"
rabbit = Woof()
print(rabbit._Woof__ribbit)

class Triangle:
    def __init__(self, side):
        self.side = side

    def adding(self):
        return self.side + self.side

    @classmethod
    def hi(cls):
        return cls("five")
apple = Triangle.hi()
print(apple.adding())

class Puppy:
    def __init__(self, a):
        self.a = a
    def ball(self):
        return "I wish bones" + self.a
    @classmethod
    def cat(cls):
        return cls("nonono")
bork = Puppy.cat()
print(bork.ball())
purr = Puppy("MEOOW")
print(purr.a)
print(purr.ball())
print(purr.cat())
print(purr)

class Flowers:
    def __init__(self, kind):
        self.kind = kind
    @staticmethod
    def correct(kind):
        if kind == "daisy":
            return "yes yippee"
        else:
            return "nononono"
boquet = ["rose", "daisy"]
if any(Flowers.correct(i) for i in boquet):
    print("this will do")
    bundle = Flowers(boquet)


