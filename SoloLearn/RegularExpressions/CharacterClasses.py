import re

#semiworking
def datecheck(s):
    if s[0] == '0':
        if s[3] == '3':
            pattern = r"0[1-9]/3[0-1]/[1-2][0-9][0-9][0-9]"
        else:
            pattern = r"0[1-9]/[0-2][0-9]/[1-2][0-9][0-9][0-9]"
    else:
        if s[3] == '3':
            pattern = r"1[0-2]/3[0-1]/[1-2][0-9][0-9][0-9]"
        else:
            pattern = r"1[0-2]/[0-2][0-9]/[1-2][0-9][0-9][0-9]"
    if re.match(pattern, s):
        return "yay"
    else:
        return "nay"

print(datecheck("02/30/2009"))
print(datecheck("13/49/10"))

#not a character class test
def q(p):
    if re.search("q", p):
        pattern = r"qu"
        if re.match(pattern, p):
            print("cool")
q("quail")
q("qwerty")
q("pique")

