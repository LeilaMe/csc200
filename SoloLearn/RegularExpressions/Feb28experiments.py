import re

#MoreMetacHaraCafters
pattern = r"kitten(!)+"

if re.match(pattern, "kitten!!!!!!"):
    print("matched")

if re.match(pattern, "kitten"):
    print("matched2")

pattern = r"^r(rain)*"

if re.match(pattern, "it is raining"):
    print("rainin")
if re.match(pattern, "rainrain"):
    print("rainnnn")
if re.match(pattern, ""):
    print("norain")
if re.match(pattern, "rollypollie"):
    print("ok")

pattern = r"the colo(u)?r gr(e|a)y"

if re.match(pattern, "the color gray is cool"):
    print("yup")

if re.match(pattern, "wow"):
    print("wow")

if re.match(pattern, "the colour grey is my house"):
    print("cool house")

pattern = r"A{1,}!"
if re.match(pattern, "AAAAAAAAaaaaaaaaa!"):
    print("run")
if re.match(pattern, "AAA!"):
    print("A")
pattern = r"[A-Ba-b]{1,}"
if re.match(pattern, "ABABBBBBAAAAaaabbaaABABbaab"):
    print("Popcorn")
if re.match(pattern, "bananas"):
    print("no bananas")
if re.match(pattern, "gumdropabaabba"):
    print("noooooooooooooooooo")

pattern = r"?P<cat>[(kitty)(cat)]
