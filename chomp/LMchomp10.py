from gasp import *

GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

CHOMP_COLOR = color.YELLOW
CHOMP_SIZE = GRID_SIZE * 0.8
CHOMP_SPEED = 0.25

FOOD_COLOR = color.RED
FOOD_SIZE = GRID_SIZE * 0.15

#my maze shape. % = wall, . = food, o = Capsule, G = ghost, P - chomp
the_layout = [
        "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", 
        "%.....o..%.....%........%%....%", 
        "%.%%%%%%...%.%...%%%%%%....%%.%", 
        "%...%....%%..%%.%%......%%%%..%", 
        "%%%...%%....%%%....%%%%......%%", 
        "%...%.%%.%%.....%%..%...%%.%..%", 
        "%.%.........%%% %%%.%.%%%...%.%", 
        "%.%%%.%%%%.%%GG GG%.%...%.%...%", 
        "%.....%.....%%%%%%%...%...%.%.%",
        "%.%%.%%.%%...%.o..%.%.%%%...%.%", 
        "%.%..%...%.%.%.%%...%..%..%%%.%",
        "%o%.%%.%.%........%..%...%%%..%", 
        "%.%.%..%.%%%%%%..%%%..%%.....%%", 
        "%.....%%.......P%%%%%....%%%.o%", 
        "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]

class Maze:
    def __init__(self):
        self.have_window = False
        self.game_over = False
        self.set_layout(the_layout)
        set_speed(20)
    
    def set_layout(self, layout):
        height = len(layout)
        width = len(layout[0])
        self.make_window(width, height)
        self.make_map(width, height)
        self.movables = []
        self.food_count = 0

        max_y = height - 1
        for x in range(width):
            for y in range(height):
                char = layout[max_y - y][x]
                self.make_object((x, y), char)
        for movable in self.movables:
            movable.draw()

    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width, screen_height, "Chomp", BACKGROUND_COLOR)
    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN
        y = y*GRID_SIZE + MARGIN
        return (x, y)
    def make_map(self, width, height):
        self.width = width
        self.height = height
        self.map = []
        for y in range(width):
            new_row = []
            for x in range(width):
                new_row.append(Nothing())
            self.map.append(new_row)
    def make_object(self, point, character):
        (x, y) = point
        if character == '%':
            self.map[y][x] = Wall(self, point)
        elif character == 'P':
            chomp = Chomp(self, point)
            self.movables.append(chomp)
        elif character == ".":
            self.food_count = self.food_count + 1
            self.map[y][x] = Food(self, point)
    def finished(self):
        return self.game_over
    def play(self):
        for movable in self.movables:
            movable.move()
        update_when('next_tick')
    def done(self):
        end_graphics()
        self.map = []
        self.movables = []
    def object_at(self, point):
        (x, y) = point
        if y < 0 or y >= self.height:
            return Nothing()
        if x < 0 or x >= self.width:
            return Nothing()
        return self.map[y][x]

    def remove_food(self, place):
        (x, y) = place
        self.map[y][x] = Nothing()
        self.food_count = self.food_count - 1
        if self.food_count == 0:
            self.win()
    def win(self):
        print("You win!")
        self.game_over = True

class Immovable:
    def is_a_wall(self):
        return False
    def eat(self, chomp):
        pass

class Nothing(Immovable):
    pass

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()
    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = GRID_SIZE * 0.2
        Circle(self.screen_point, dot_size, color=WALL_COLOR, filled=True)
        (x, y) = self.place
        neighbors = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
        for neighbor in neighbors:
            self.check_neighbor(neighbor)
    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)
        if object.is_a_wall():
            here = self.screen_point
            there = maze.to_screen(neighbor)
            Line(here, there, color=WALL_COLOR, thickness=2)
    def is_a_wall(self):
        return True

class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze
        self.place = point
        self.speed = speed
    def furthest_move(self, movement):
        (move_x, move_y) = movement
        (current_x, current_y) = self.place
        nearest = self.nearest_grid_point()
        (nearest_x, nearest_y) = nearest
        maze = self.maze

        if move_x > 0:
            next_point = (nearest_x+1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x > nearest_x:
                    move_x = nearest_x - current_x
        elif move_x < 0:
            next_point = (nearest_x-1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x < nearest_x:
                    move_x = nearest_x - current_x
        if move_y > 0:
            next_point = (nearest_x, nearest_y+1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y > nearest_y:
                    move_y = nearest_y - current_y
        elif move_y < 0:
            next_point = (nearest_x, nearest_y-1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y < nearest_y:
                    move_y = nearest_y - current_y
        if move_x > self.speed:
            move_x = self.speed
        elif move_x < -self.speed:
            move_x = -self.speed
        if move_y > self.speed:
            move_y = self.speed
        elif move_y < -self.speed:
            move_y = -self.speed
        return (move_x, move_y)
    def nearest_grid_point(self):
        (current_x, current_y) = self.place
        grid_x = int(current_x + 0.5)
        grid_y = int(current_y + 0.5)
        return (grid_x, grid_y)

class Chomp(Movable):
    def __init__(self, maze, point):
        self.direction = 0
        Movable.__init__(self, maze, point, CHOMP_SPEED)
    def move(self):
        keys = keys_pressed()
        if 'left' in keys: self.move_left()
        elif 'right' in keys: self.move_right()
        elif 'up' in keys: self.move_up()
        elif 'down' in keys: self.move_down()
    def move_left(self):
        self.try_move((-1, 0))
    def move_right(self):
        self.try_move((1, 0))
    def move_up(self):
        self.try_move((0, 1))
    def move_down(self):
        self.try_move((0, -1))
    
    def try_move(self, move):
        (move_x, move_y) = move
        (current_x, current_y) = self.place
        (nearest_x, nearest_y) = (self.nearest_grid_point())
        if self.furthest_move(move) == (0, 0):
            return
        if move_x != 0 and current_y != nearest_y:
            move_x = 0
            move_y = nearest_y - current_y
        elif move_y != 0 and current_x != nearest_x:
            move_y = 0
            move_x = nearest_x - current_x
        move = self.furthest_move((move_x, move_y))
        self.move_by(move)
    def draw(self):
        maze = self.maze
        screen_point = maze.to_screen(self.place)
        angle = self.get_angle()
        endpoints = (self.direction + angle, self.direction + 360 - angle)
        self.body = Arc(screen_point, CHOMP_SIZE, endpoints[0], endpoints[1], filled=True, color = CHOMP_COLOR)
    def get_angle(self):
        (x, y) = self.place
        (nearest_x, nearest_y) = (self.nearest_grid_point())
        distance = (abs(x-nearest_x) + abs(y-nearest_y))
        return 1 + 90*distance
    def move_by(self, move):
        self.update_position(move)
        old_body = self.body
        self.draw()
        remove_from_screen(old_body)

        (x, y) = self.place
        nearest_point = self.nearest_grid_point()
        (nearest_x, nearest_y) = nearest_point
        distance = (abs(x-nearest_x) + abs(y-nearest_y))

        if distance < self.speed * 3/4:
            object = self.maze.object_at(nearest_point)
            object.eat(self)

    def update_position(self, move):
        (old_x, old_y) = self.place
        (move_x, move_y) = move
        (new_x, new_y) = (old_x+move_x, old_y+move_y)
        self.place = (new_x, new_y)

        if move_x > 0:
            self.direction = 0
        elif move_y > 0:
            self.direction = 90
        elif move_x < 0:
            self.direction = 180
        elif move_y < 0:
            self.direction = 270

class Food(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()
    def draw(self):
        self.dot = Circle(self.screen_point, FOOD_SIZE, color=FOOD_COLOR, filled=True)
    def eat(self, chomp):
        remove_from_screen(self.dot)
        self.maze.remove_food(self.place)

the_maze = Maze()

while not the_maze.finished():
    the_maze.play()

the_maze.done()
