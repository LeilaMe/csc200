# Given positive integer K, compute the number of legal pairings of
# velociraptors with their hunting partners, MODULO 10007
# --> store this value in numConfigurations

List = ["Doggy", "Kitty", "Bob", "Penny", "Cake"]
def function():
    new = []
    for a in List:
        for b in List[List.index(a):]:
            if a == b:
                pass
            else:
                new.append(a + " and " + b)
    print(new)
    print(len(new))
function()

def getConfigurationCount(K):
    if K > 0:
        numConfigurations = -1
        var = 1
        #for i in range(1, K+1):
        i = K
        while i > 0:
            var *= (i)
            i -= 1
        return var
        return numConfigurations

print(getConfigurationCount(5))

#Divide
#Divide numerator by denominator and store the resulting integer in
#"result" -- you can assume numerator is divisible by denominator

def divideWithoutDivide(numerator, denominator):
    if numerator >= 0:
        if denominator > 0:
            result = 0
            while numerator >= denominator:
                result += 1
                numerator = numerator - denominator
    return result
print(divideWithoutDivide(10, 2))
print(divideWithoutDivide(20, 5))
print(divideWithoutDivide(2, 10))

#Flipping
# Store the L final indices in this array
# Indices should be stored in the order that they would've been
# flipped by the arm, that is, the 0th index would've been flipped
# before the 1st index .. before the L-1st and final index

def findUnflippedIndices(M, K, L):
    if M > 0:
        if K > 0 and K <= M:
            if L > 0 and K <=M:
                finalPositions = []
                finalPositions.append(L)
                finalPositions.append(K)
                finalPositions.append(M)
    return finalPositions

print(findUnflippedIndices(100, 7, 3))

def multiplyrationals(one, two):
    product = 0
    for a in range(one):
        product += two
    return product

print(multiplyrationals(2, 4))
print(multiplyrationals(-9, 3))

def multrats(onenum, oneden, twonum, twoden):
    pronum = 0
    proden = 0
    for a in range(onenum):
        pronum += twonum
    for b in range(oneden):
        proden += twoden
    return pronum, proden
#1/2 * 3
print(multrats(1, 2, 3, 1))
#50/3 * 3/50
print(multrats(50, 3, 3, 50))


